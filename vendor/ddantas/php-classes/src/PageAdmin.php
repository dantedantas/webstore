<?php
/**
 * Created by PhpStorm.
 * User: dante
 * Date: 18.08.2017
 * Time: 22:26
 */

namespace DClass;


class PageAdmin extends Page
{

    public function __construct($opts = array(), $tpl_dir = "/views/admin/")
    {

        parent::__construct($opts, $tpl_dir);

    }

}